use super::*;

#[derive(Debug, Serialize)]
pub struct Wlan {
	name: Option<String>,
	ip: Option<String>,
	rssi: isize,
	con: bool,
	rfkill: bool,
	#[serde(skip_serializing)]
	internet: bool,
}

impl Wlan {
	pub fn new() -> Wlan {
		Wlan {
			name: None,
			ip: None,
			rssi: 0,
			con: false,
			rfkill: false,
			internet: false,
		}
	}
	pub fn update(&mut self) {
		let output =
			match Command::new("iwctl")
				.arg("station")
				.arg("wlan0")
				.arg("show")
				.output() {
				Ok(output) => output.stdout,
				Err(_) => return,
			};
		let output = String::from_utf8_lossy(&output);

		if "No station on device: 'wlan0'\n" == &output {
			self.name = None;
			self.ip = None;
			self.rssi = 0;
			self.con = false;
			self.rfkill = false;
			self.internet = false;
		} else {
			let mut state = None;
			for line in output.lines() {
				if line.contains("State") {
					state = Some(line[34..].trim())
				}
			}
			if state.is_some() {
				if "connected" == state.unwrap() {
					for line in output.lines() {
						if line.contains("Connected network") {
							self.name = Some(line[34..].trim().to_string());
						}
						if line.contains("IPv4 address") {
							self.ip = Some(line[34..].trim().to_string());
						}
						if line.contains("AverageRSSI") {
							self.rssi = line[34..].split_whitespace().next().unwrap()
								.parse::<isize>().unwrap() + 100;
						}
					}
					self.con = true;
					self.rfkill = true;
				} else {
					self.name = None;
					self.rssi = 0;
					self.con = false;
					self.rfkill = true;
					self.internet = false;
				}
			}
		}
	}
}
