extern crate serde;
use serde::Serialize;
use glob::glob;

use std::{
	path::PathBuf,
	fs,
	thread,
	sync::mpsc,
	time::Duration,
	collections::HashMap,
	error::Error,
	process::Command,
};

mod cpu;
use cpu::*;

mod gpu;
use gpu::*;

mod mem;
use mem::*;

mod wlan;
use wlan::*;

mod bt;
use bt::*;

//mod proc;
//use proc::*;

mod consts;
use consts::*;

#[derive(Debug, Serialize)]
pub struct Info {
	pub cpu: Cpu,
	pub gpu: Gpu,
	pub mem: Mem,
	pub swap: Mem,
	pub wlan: Wlan,
	pub bt: Bt,
}

impl Info {
	pub fn new() -> Info {
		Info {
			cpu: Cpu::new(),
			gpu: Gpu::new(),
			mem: Mem::new(),
			swap: Mem::new(),
			wlan: Wlan::new(),
			bt: Bt::new(),
		}
	}
	pub fn update(&mut self) {
		self.cpu.update();
		self.gpu.update();
		self.wlan.update();
		self.bt.update();
		println!("hello");
		self.mem.update();
		if SWAP {
			self.swap.update_swap();
		}
		println!("{}", serde_json::to_string(self).unwrap());
	}
}


pub fn parse_file<T: std::str::FromStr>(path: &String) -> Result<T, Box<dyn Error>>
	where <T as std::str::FromStr>::Err: std::error::Error, <T as std::str::FromStr>::Err: 'static
{
	Ok(fs::read_to_string(path)?
		.trim()
		.parse::<T>()?)
}


fn get_temp_path(path: &String, name: &str) -> Result<HashMap<String, String>, ()> {
	let mut out = HashMap::new();
	let mut get_list = |tail: &str| -> Result<(), ()> {
		match glob(&(format!("{}hwmon/hwmon*/*_{}", path, tail))) {
			Ok(glob) => {
				for entry in glob {
					if let Ok(path) = entry {
						if fs::read_to_string(path.parent().unwrap().join("name")).unwrap().trim() != name {
							continue
						}
						let mut path2 = path.display().to_string();
						for _ in 0..tail.len() {
							path2.pop();
						}
						let temp_name = parse_file(&format!("{}label", &path2)).unwrap_or({
							let mut str = path2.split('/').last().unwrap().to_string();
							str.pop();
							str
						});
						let path_name = path.display().to_string();
						out.insert(temp_name, path_name);
					};
				};
				Ok(())
			},
			Err(_) => Err(())
		}
	};
	get_list("input")?;
	get_list("average")?;
	Ok(out)
}


#[derive(Debug, Serialize)]
pub struct Temp {
	#[serde(skip_serializing)]
	path: String,
	temp: f32,
}


fn parse_line(line: &str) -> f32 {
	line.split_whitespace()
		.nth(1)
		.unwrap()
		.trim()
		.parse::<f32>()
		.unwrap() / (1024.0*1024.0)
}
