use get_sysinfo::Info;

use std::{
	time::Duration,
	thread::sleep,
};

fn main() {
	let mut info = Info::new();
	loop {
		info.update();
		sleep(Duration::from_secs(1));
	}
}
