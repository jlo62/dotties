**Greetings, Wanderer**
======
This is my personnal dotfiles backup, so I haven't set this up for portability. But feel free to use it!.

If you like it, please star this repo to give me some feedback.

If you have any tips, ideas or suggestions, or want other dotfiles, [open a ticket](https://gitlab.com/jlo62/dotties/-/issues) or [write me a mail](mailto:janluca.oster@proton.me).\
Don't be shy, I don't bite :D

![](./.screenshots/empty.png)
![](./.screenshots/sirula.png)
![](./.screenshots/geany_srain_nf.png)

What
------

* **OS: [Arch Linux](https://archlinux.org)**: Very customizable distro with no default DE, + pacman and the AUR.
* **WM: [sway](https://swaywm.org/)/[swayfx](https://github.com/WillPower3309/swayfx)**: a lightweight tiling window manager. Swayfx adds rounded corners, but I'm waiting for it to rebase off v`1.10`.
* **Bar: [eww](https://github.com/elkowar/eww)**: *Very* customizable widget system, even supports graphs and a tray.
* **Launcher: [sirula](https://github.com/jlo62/sirula)** (my own fork): Fast and lightweight launcher with a excelent search feature. It just can't open files and doesn't have a daemon mode.
* **Terminal: [foot](https://codeberg.org/dnkl/foot)**: *Very* lightweight, cpu-based terminal emulator with a efficient renderer.
* **Shell: zsh**: Very customizable Shell
* **Notification daemon: [mako](https://github.com/emersion/mako)**: a lightweight notification daemon. Doesn't support animations.
* **On-screen display (OSD): [SwayOSD](https://github.com/ErikReider/SwayOSD)**: Nice on-screen display. Currently forked off my [own branch](https://github.com/jlo62/SwayOSD) for MPRIS support.
* **IDE: [geany](https://www.geany.org/)**: A lightweight, native IDE with a VTE, that doesn't have the whole KDE stack as dependencies.
* **Browser: [firefox](https://www.mozilla.org/en-US/firefox/new)**: the one and only
* **System Monitor: [missioncenter](https://gitlab.com/mission-center-devs/mission-center)**: It's really fancy shnancy

Installation
------

These dotfiles were backed up following [the ArchWiki](https://wiki.archlinux.org/title/Dotfiles)

To move overlapping files to `.dotfiles-backup` (not tested if this works):
```
git clone --bare https://gitlab.com/jlo62/dotties $HOME/.dotfiles
alias dotfiles='/usr/bin/git --git-dir="$HOME/.dotfiles/" --work-tree="$HOME"'
mkdir -p .dotfiles-backup && \
dotfiles checkout [[branch]] 2>&1 | egrep "\s+\." | awk {'print $1'} | \
xargs -I{} mv {} .dotfiles-backup/{}
```
replace `[[branch]]` with the prefered branch, or remove to use the default.

### sway
edit `~/.config/sway/devices.conf` for all device-relevant things.

The cursor theme is [bibata](https://github.com/ful1e5/Bibata_Cursor) which must be installed seperately.

See `~/.config/sway/autostart.conf` for automatically executed commands.

### eww

eww requires some configuration, as parameters for the scripts are compiled in.

The file `~/.config/eww/code/get_sysinfo/src/consts.rs` has the following variables:
```
# which GPU to monitor. 'readlink -f /sys/class/drm/card?/device' will list all, choose one.
pub const GPU_PATH: &str = "/sys/devices/pci0000:00/0000:00:03.1/0000:2b:00.0/0000:2c:00.0/0000:2d:00.0/";

# how many threads to monitor. 'nproc'
pub const CPU_THREADS: usize = 24;
# which hwmon driver to read. typically k10temp for AMD systems. 'cat /sys/class/hwmon/hwmon*/name'
pub const CPU_HWMON_NAME: &str = "zenpower";
# Which CPU temperature to take. If you don't know what this is leave it.
pub const CPU_HWMON_LABEL: &str = "Tctl";
# Where to source the CPU power draw. 'for i in /sys/class/powercap/intel-rapl*/energy_uj; do echo $(cat ${i%/*}/name): echo $i; done'
pub const CPU_ENERGY_UJ_PATH: &str = "/sys/class/powercap/intel-rapl:0/energy_uj";

# do you have swap?
pub const SWAP: bool = false;
```

Install the new binarys (the existing ones are optimized for zen3, expect them to crash on other Architectures):
```
cd ~/.config/eww/code/;
for i in ./*; do
	cargo install --path $i --force
done
```
This will install the files to `~/.config/eww/bin/`.

It might also require editing `bar.yuck`/`bar/info.yuck`. glhf, the syntax isn't *too* hard.

Hardware
------
* **PC**: 5900X/7800XT/64GB/A
* **Laptop**: 6800u/iGPU/16GB
