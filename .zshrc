source ~/.config/zsh/comp
#source ~/.config/zsh/private

prompt="%F{cyan}%n %2~%f%(1j. [%F{cyan}%j%f].)%(0?..%(130?.. (%F{#FF0000}%?%f%))) %F{#00FF00}:%f " 


### aliases
alias dotfiles='/usr/bin/git --git-dir="$HOME/.dotfiles/" --work-tree="$HOME"'
alias grep='grep --color=auto'
alias ls='ls -AlGh --color=auto'
alias paru="taskset -c 6-23 nice -n 19 paru"
alias makepkg="taskset -c 4-23 nice -n 19 makepkg"

alias upload='curl -sSF "file=@-" 0x0.st | wl-copy -n; notify-send upload "$(wl-paste -n) - copied"'
alias clipupload='wl-paste -n | curl -sSF "file=@-" 0x0.st | wl-copy -n; notify-send clipupload "$(wl-paste -n) - copied"'
alias userctl='/usr/bin/systemctl --user'
alias hg='history | grep'

alias root="sudo -i"

for i in \
	mkinitcpio modprobe mount pacman systemctl dmesg umount swapon swapoff downgrade
do
	alias $i="sudo $i"
done

### variables
export PATH="$HOME/.local/bin:$PATH:$HOME/.cargo/bin:/usr/share/sway-contrib"

export ADW_DISABLE_PORTAL=1

export GSK_RENDERER=gl
export GDK_DEBUG=gl-no-fractional

export ELECTRON_OZONE_PLATFORM_HINT=auto
export _JAVA_AWT_WM_NONREPARENTING=1

export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share
export XDG_STATE_HOME=$HOME/.local/state

export XDG_DATA_DIRS=/usr/local/share:/usr/share
export XDG_CONFIG_DIRS=/etc/xdg

if [ "$(tty)" = "/dev/tty1" ]; then
	export XDG_CURRENT_DESKTOP=sway
	exec sway
fi

autoload -U history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end
bindkey "^[[A" history-beginning-search-backward-end
bindkey "^[[B" history-beginning-search-forward-end

bindkey  "^[[H"   beginning-of-line
bindkey  "^[[F"   end-of-line
bindkey  "^[[3~"  delete-char

HISTFILE=~/.config/zsh/histfile
HISTSIZE=16000
SAVEHIST=16000
setopt autocd extendedglob notify
unsetopt beep nomatch


# reload zsh on install
TRAPUSR1() { rehash }
